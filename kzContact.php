<?php
if(!defined('PLX_ROOT')) {
	header('Content-Type: text/plain; charset=utf-8');
	readfile('hymne-a-la-beaute.txt');
	exit;
}

/**
 * Plugin myContact
 * @author: Bazooka07
 * from plxMyContact written by	Stephane F
 * @lastdate: 2020-03-26
 * */
class kzContact extends plxPlugin {

	const PREFIX_CONTACT = 'form';

	public function __construct($default_lang) {
		parent::__construct($default_lang);

		self::setConfigProfil(PROFIL_ADMIN);

		if(function_exists('mail')) {
			$hooks = array(
				'plxMotorPreChauffageBegin',
				'plxMotorDemarrageBegin',
				'plxShowConstruct',
				'plxShowPageTitle',
				'plxShowStaticListEnd'
			);
			foreach($hooks as $hook) {
				self::addHook($hook, $hook);
			}
		}

	}

	private function __mail($to, $subject, $body, $cc, $bcc) {
		$aHeaders = array(
			'MIME-Version'				=> '1.0',
			'Content-Type'				=> 'text/plain ;charset=' . PLX_CHARSET,
			'Content-Transfer-Encoding'	=> '8bit',
			'Date'						=> date('D, j M Y G:i:s O'), # Sat, 7 Jun 2001 12:35:58 -0700
			'X-Mailer'					=> 'PHP/' . phpversion()
		);

		if(empty($cc)) {
			$aHeaders['Cc'] = (is_array($cc)) ? implode(', ', $cc) : $cc;
		}

		if(empty($bcc)) {
			$aHeaders['Bcc'] = (is_array($bcc)) ? implode(', ', $bcc) : $bcc;
		}

		if(version_compare(PHP_VERSION, '7.2', '>=')) {
			$headers = $aHeaders;
		} else {
			$headers = implode("\r\n", array_map(
				function($k, $v) { return "$k: $v"; },
				array_keys($headers),
				array_values($headers)
			)) . "\r\n";
		}

		return mail($to, $subject, $body, $headers);
	}

	/**
	 * Envoi par courriel le POST du formulaire de contact avec
	 * les renseignements sur l'expéditeur fournis par le serveur.
	 * */
	public function sendMessage($inputs) {

		if(!empty(self::getParam('capcha'))) {
			$capcha = $_SESSION['capcha'];
			$rep = filter_input(INPUT_POST, 'rep', FILTER_SANITIZE_STRING);
			if(empty($capcha) or $capcha != sha1($rep)) {
				# Erreur de vérification capcha
				return self::getLang('L_ERR_ANTISPAM');
			}
		}

		if(empty($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
			// anti-spam
			return parent::getLang('L_ERR_CONTEXT');
		} else {
			# envoi des courriels
			$subject = sprintf(self::getParam('subject'), 'https://' . $_SERVER['HTTP_HOST']);

			ob_start();
			foreach($inputs as $field=>$value) {
				echo (preg_match('@(?:message)$@', $field)) ? PHP_EOL : parent::getLang('L_'.strtoupper($field)) . ' : ';
				echo $value . PHP_EOL;
			}
			echo str_repeat('-', 40) . PHP_EOL;
			foreach(array('HTTP_REFERER', 'REMOTE_ADDR', 'HTTP_USER_AGENT', 'HTTP_ACCEPT_LANGUAGE', 'HTTP_ACCEPT') as $field) {
				if(!empty($_SERVER[$field])) {
					$caption = ucfirst(str_replace('_', '-', str_replace('HTTP_', '', $field)));
					$value = filter_input(INPUT_SERVER, $field, FILTER_SANITIZE_STRING);
					echo $caption . ': ' . $value . PHP_EOL;
				}
			}

			if(!empty(self::getParam('envoiSepare'))) {
				$recipients = array_merge(
					explode(',', self::getParam('email')),
					explode(',', self::getParam('email_cc'))
				);
				if (count($recipients) > 1) {
					echo PHP_EOL . self::getLang('L_SENT_TO') . ':' . PHP_EOL . implode(', ', $recipients);
				}
			}

			echo PHP_EOL . 'Sent by kzContact plugin' . PHP_EOL;

			$body = str_replace(PHP_EOL, "\r\n", ob_get_clean());

			if (!empty($recipients) and count($recipients) > 1) {
				$succes = false;
				foreach(array_merge($recipients, explode(',', self::getParam('email_cc'))) as $to) {
					if(self::__mail(
						trim($to),
						$subject,
						$body
					)) {
						$success = true;
					}
				}
				if(empty($success)) {
					return self::getLang('L_ERR_SENDMAIL');
				}
			} elseif(!self::__mail(
				self::getParam('email'),
				$subject,
				$body,
				self::getParam('email_cc'),
				self::getParam('email_bcc')
			)) {
				return self::getLang('L_ERR_SENDMAIL');
			}
		}
		return false; # Pas d'erreur
	}

	/* ==================== Hooks ====================== */

	public function plxMotorPreChauffageBegin() {
		# Le traitement ne s'effectuera que pour l'affichage du formulaire de contact
		echo '<?php' . PHP_EOL;
?>
if(!empty($this->get) and $this->get == '<?= self::getParam('content') ?>') {
	$this->mode = '<?= self::getParam('content') ?>';
	$this->cible = str_repeat('../', substr_count($this->aConf['racine_statiques'], '/')).$this->aConf['racine_plugins'].'<?= __CLASS__.'/'.self::PREFIX_CONTACT ?>';
	$this->template = '<?= self::getParam('template')=='' ? 'static.php' : self::getParam('template') ?>';
	return true;
}
<?php
		echo PHP_EOL . '?>';
	}

	public function plxMotorDemarrageBegin() {
		echo '<?php' . PHP_EOL;
?>
if($this->mode == '<?= self::getParam('content') ?>') { return true; }
<?php
		echo PHP_EOL . '?>';
	}

	// infos sur la page statique
	public function plxShowConstruct() {

		# dans plxShow::staticContent()
		# $file = PLX_ROOT.$this->plxMotor->aConf['racine_statiques'].$this->plxMotor->cible;
		# $file .= '.'.$this->plxMotor->aStats[ $this->plxMotor->cible ]['url'].'.php';
		# Le traitement ne s'effectuera que pour l'affichage du formulaire de contact
		$form = self::getParam('content');
		echo '<?php' . PHP_EOL;
?>
if($this->plxMotor->mode=='<?= $form ?>') {
	$statique = array(
		$this->plxMotor->cible=>array(
			'name'		=> '<?= addslashes(self::getParam('mnuName')) ?>',
			'url'		=> '<?= $form ?>',
			'active'	=> 1,
			'menu'		=> '',
			'readable'	=> 1
		)
	);
	$this->plxMotor->aStats = array_merge($this->plxMotor->aStats, $statique);
}
<?php
		echo PHP_EOL . '?>';
	}

	/**
	 * Affiche une option pour le formulaire de contact dans le menu de navigation.
	 * */
	public function plxShowStaticListEnd() {

		if(!empty(self::getParam('mnuDisplay'))) {
			# ajout au menu pour accéder à la page de contact
			$form = self::getParam('content');
			echo '<?php' . PHP_EOL;
?>
	$href = $this->plxMotor->urlRewrite('?<?= $form ?>');
	$class = ($this->plxMotor->mode=='<?= $form ?>') ? 'active':'noactive';
	$pattern = <<< PATTERN
<li class="static menu $class"><a href="$href"><?= addslashes(self::getParam('mnuName')) ?></a></li>
PATTERN;
	array_splice($menus, <?= intval(self::getParam('mnuPos')) - 1 ?>, 0, $pattern);
<?php
		echo PHP_EOL . '?>';
		}
	}

	/**
	 * Affiche le titre de la page pour le formulaire de contact.
	 * */
	public function plxShowPageTitle() {
		$title = self::getParam('title_htmltag');
		if (empty($title)) { $title = self::getParam('mnuName'); }
		if (!empty($title)) { $title .= ' - '; }
		echo '<?php' . PHP_EOL;
?>
if($this->plxMotor->mode == '<?= self::getParam('content') ?>') {
	echo '<?= $title ?>'.plxUtils::strCheck($this->plxMotor->aConf['title']);
	return true;
}
<?php
		echo PHP_EOL . '?>';
	}
}
?>
