<?php
if(!defined('PLX_ADMIN') and isset($error) and $error === false) {
	# Courriel envoyé
?>
			<p class="success"><?php echo nl2br(plxUtils::strCheck($plxPlugin->getParam('thankyou'))); ?></p>
			<p><input type="button" value="Retour à l'accueil" onclick="window.location.replace('<?php echo $this->plxMotor->racine; ?>');" /></p>
<?php
} else {
	if(defined('PLX_ADMIN')) {
		# côté admin
		if(function_exists('mail')) {
			$color = 'green'; $msg = $plxPlugin->getLang('L_MAIL_AVAILABLE');
		} else {
			$color = 'red'; $msg = $plxPlugin->getLang('L_MAIL_NOT_AVAILABLE');
		}
?>
		<p style="color:<?= $color ?>"><strong><?= $msg ?></strong></p>
<?php
	} else {
		# côté site
		if(!empty($error)) {
			# Il y a une erreur
?>
				<p class="contact_error"><?= $error ?></p>\n
<?php
		}
		$mnuNameC = $plxPlugin->getParam('mnuNameC');
		if(! empty($mnuNameC)) {
			echo <<< EOT
		<p class="mycontact">$mnuNameC</p>
EOT;
		}
		$mnuAdress = $plxPlugin->getParam('mnuAdress');
		if(! empty($mnuAdress)) {
			echo <<< EOT
		<p class="mycontact">$mnuAdress</p>
EOT;
		}
		$mnuPostCode = $plxPlugin->getParam('mnuPostCode');
		if(! empty($mnuPostCode)) {
			echo <<< EOT
		<p class="mycontact">$mnuPostCode</p>
EOT;
		}
		$mnuTown = $plxPlugin->getParam('mnuTown');
		if(! empty($mnuTown)) {
			echo <<< EOT
		<p class="mycontact">$mnuTown</p>
EOT;
		}
		$mnuEmail = $plxPlugin->getParam('mnuEmail');
		if(! empty($mnuEmail)) {
			echo <<< EOT
		<p class="mycontact">$mnuEmail</p>
EOT;
		}
		$mnuTel = $plxPlugin->getParam('mnuTel');
		if(! empty($mnuTel)) {
			echo <<< EOT
		<p class="mycontact">$mnuTel</p>
EOT;
		}
		$mnuText = $plxPlugin->getParam('mnuText');
		if(! empty($mnuText)) {
			echo <<< EOT
		<h4>$mnuText</h4>
EOT;
		}
	}
?>

			<form class="inline-form kz-contact" name="kzcontact" method="post">
			<fieldset>
<?php
		foreach ($params as $field=>$infos) {
			if(!empty($infos['break-before'])) {
?>
			</fieldset><fieldset>
<?php
			}
			$id = 'id_' . $field;
			$class = ($infos['type'] == 'textarea' or !empty($infos['large'])) ? 'class="large"' : '';
			$caption = $plxPlugin->getLang('L_'.strtoupper($field));
?>
				<div <?= $class ?>>
					<!--label for="<?= $id ?>"><?= $caption ?></label-->
<?php
			if(defined('PLX_ADMIN')) {
				$value = $plxPlugin->getParam($field);
				if(empty($value) and !empty($infos['default'])) {
					$value = $infos['default'];
				}
			} else {
				$value = (filter_has_var(INPUT_POST, $field)) ? filter_input(INPUT_POST, $field, FILTER_SANITIZE_STRING) : '';
				if(!is_string($value)) {
					$value = '';
				}
			}

			$required = (!empty($infos['required'])) ? ' required' : '';
			$extras = (!empty($infos['extras'])) ? ' ' . implode(' ', array_map(
				function($a, $b) {
					return <<< EXTRAS
$a="$b"
EXTRAS;
				},
				array_keys($infos['extras']),
				array_values($infos['extras'])
			)) : '';
			switch($infos['type']) {
				case 'textarea':
?>
					<textarea id="<?= $id ?>" name="<?= $field ?>" placeholder="<?= $caption ?>"<?= $extras ?><?= $required ?>}><?= $value ?></textarea>
<?php
					break;
				case 'select' :
					$options = implode("\n", array_map(function($option, $caption) use($value) {
						$selected = ($option == $value) ? ' selected' : '';
						return <<< OPTION
							<option value="$option"$selected>$caption</option>
OPTION;
					}, array_keys($infos['select']), array_values($infos['select'])));
?>
					<select id="<?= $id ?>" name="<?= $field ?>"<?= $extras ?>>
<?= $options ?>
					</select>
<?php
					break;
				case 'checkbox':
					$checked = (!empty($value)) ? ' checked' : '';
?>
					<input type="checkbox" id="<?= $id ?>" name="<?= $field ?>" value="1"<?= $extras ?><?= $checked ?> />
<?php
					break;
				default:
					$multiple = ($infos['type'] == 'email' and !empty($infos['multiple'])) ? ' multiple' : '';
?>
					<input type="<?= $infos['type'] ?>" id="<?= $id ?>" name="<?= $field ?>" value="<?= $value ?>" placeholder="<?= $caption ?>"<?= $extras ?><?= $required?><?= $multiple ?> />
<?php
			}

?>
				</div>
<?php
		}

$className = defined('PLX_ADMIN') ? 'in-action-bar"' : 'kz-contact-footer';
?>
				<div class="<?= $className ?>">
					<?php echo plxToken::getTokenPostMethod(); ?>
<?php
$isCapcha = (!defined('PLX_ADMIN') and !empty($plxPlugin->getParam('capcha')));
if($isCapcha) {
	$this->plxMotor->plxCapcha = new plxCapcha(); # Création objet captcha
?>

					<?php $this->capchaQ(); ?>

					<span class="spacer">&nbsp;</span>
					<input id="id_rep" name="rep" type="text" size="6" maxlength="2" required />
<?php
} else {
?>
					<span class="spacer">&nbsp;</span>
<?php
}
?>
					<input type="submit" class="blue"/>
				</div>
			</fieldset>
		</form>
		<!-- https://loading.io/css/ -->
		<div id="spinner" class="kz-contact lds-ellipsis">
			<div></div>
			<div></div>
			<div></div>
			<div></div>
		</div>
		<script src="plugins/kzContact/script.min.js" type="application/javascript"></script>

<?php
}
?>
