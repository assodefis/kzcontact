<?php
$LANG = array(
	'L_CAPCHA'	=> 'Aktivieren Sie Anti-Captcha',
	'L_CONTENT'	=> 'Formular zu verwenden',
	'L_DEFAULT_MENU_NAME'	=> 'Kontakt',
	'L_DEFAULT_OBJECT'	=> 'Neuer Kontakt von Ihrer Website% s',
	'L_DEFAULT_THANKYOU'	=> 'Vielen Dank, dass Sie mich kontaktiert haben. Ich werde so schnell wie möglich antworten.',
	'L_EMAIL'	=> 'Empfänger E-Mail *',
	'L_EMAIL_BCC'	=> 'Blinde Kopie der E-Mail-Empfänger *',
	'L_EMAIL_CC'	=> 'Durchschlag der E-Mail-Empfänger *',
	'L_EMAIL_SUBJECT'	=> 'E-Mail Betreff',
	'L_ENVOISEPARE'	=> 'E-Mail nacheinander senden',
	'L_ERROR_EMAIL'	=> 'Bitte geben Sie eine gültige Email-Adresse ein',
	'L_ERR_ANTISPAM'	=> 'Anti-Spam-Überprüfung fehlgeschlagen',
	'L_ERR_CONTENT'	=> 'Bitte geben Sie den Inhalt Ihrer Nachricht ein',
	'L_ERR_CONTEXT'	=> 'Wir können nicht auf Ihre Anfrage zugreifen. Bitte verwenden Sie einen anderen Weg.',
	'L_ERR_EMAIL'	=> 'Bitte geben Sie eine gültige Email-Adresse ein',
	'L_ERR_NAME'	=> 'Bitte geben Sie Ihren Namen ein',
	'L_ERR_SENDMAIL'	=> 'Beim Senden Ihrer Nachricht ist ein Fehler aufgetreten',
	'L_ERR_SINGLE_EMAIL'	=> 'Geben Sie eine richtige E-Mail-Adresse ein',
	'L_FORM_ANTISPAM'	=> 'Anti-Spam-Überprüfung',
	'L_FORM_BTN_RESET'	=> 'Zurücksetzen',
	'L_FORM_BTN_SEND'	=> 'Senden',
	'L_FORM_CONTENT'	=> 'Inhalt Ihrer Nachricht',
	'L_FORM_MAIL'	=> 'Deine Emailadresse',
	'L_FORM_NAME'	=> 'Dein Name',
	'L_GO_HOME'	=> 'Startseite',
	'L_MAIL_AVAILABLE'	=> 'Mail-Sendefunktion verfügbar',
	'L_MAIL_NOT_AVAILABLE'	=> 'Mail-Sendefunktion nicht verfügbar',
	'L_MENU_DISPLAY'	=> 'Zeigen Sie das Menü der Kontaktseite an',
	'L_MENU_POS'	=> 'Menüposition',
	'L_MENU_TEXT'	=> 'Text oben im Formular eingefügt',
	'L_MENU_TITLE'	=> 'Menütitel',
	'L_MISSING_VALUE'	=> 'Das Ausfüllen des Eintrags% s ist erforderlich',
	'L_MNUDISPLAY'	=> 'Anzeige im Menü',
	'L_MNUGROUP'	=> 'Gruppe innerhalb des Menüs',
	'L_MNUNAME'	=> 'Beschriftung im Menü',
	'L_MNUPOS'	=> 'Position im Menü',
	'L_MNUTEXT'	=> 'Kommentar auf dem Formular',
	'L_MSG_WELCOME'	=> 'Bitte füllen Sie das Formular wie folgt aus',
	'L_PAGE_TITLE'	=> 'Kontakt',
	'L_SAVE'	=> 'Speichern',
	'L_SENT_TO'	=> 'Gesendet an',
	'L_SITEMAP'	=> 'In sitemap.php verwiesen',
	'L_SUBJECT'	=> 'Betreff für die E-Mail',
	'L_TEMPLATE'	=> 'Vorlage',
	'L_THANKYOU'	=> 'Meldung wird bei Erfolg angezeigt',
	'L_THANKYOU_MESSAGE'	=> 'Danke Nachricht',
	'L_TITLE_HTMLTAG'	=> 'Titel der HTML-Seite',
	'L_YOUR-EMAIL'	=> 'E-Mail',
	'L_YOUR-FUNCTION'	=> 'Ihre Aktivität',
	'L_YOUR-MESSAGE'	=> 'Ihre Nachricht',
	'L_YOUR-NAME'	=> 'Name',
	'L_YOUR-PHONE'	=> 'Telefonnummer',
	'L_YOUR-SOCIETY'	=> 'Compagny',
	'L_YOUR-SUBJECT'	=> 'Betreff der Nachricht'
);
?>
